package pe.com.claro.jdbc.timeai.spconsultaconfiguracion.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@ConfigurationProperties(prefix = "spconsultaconf")
@Getter
@Setter
public class PropiedadesExternas {

	private String spOwner;
	private String spName;
	private int spTimeout;
			
}
