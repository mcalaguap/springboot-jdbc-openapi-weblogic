package pe.com.claro.jdbc.timeai.spconsultaconfiguracion.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import pe.com.claro.jdbc.timeai.spconsultaconfiguracion.entity.Configuracion;
import pe.com.claro.jdbc.timeai.spconsultaconfiguracion.model.ConsultaConfigRequest;
import pe.com.claro.jdbc.timeai.spconsultaconfiguracion.model.ConsultaConfigResponse;
import pe.com.claro.jdbc.timeai.spconsultaconfiguracion.service.ConfigService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Slf4j
@RestController
@RequestMapping("/api/pkgTransaccion")
public class SpConsultaConfController {

	@Autowired
	private ConfigService configService;
	
	
	@Operation(summary = "Consulta de datos configurativos para el gestor de ordenes")
    @ApiResponses(value = { 
            @ApiResponse(responseCode = "200", description = "Consulta ejecutada con exito", 
                content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = Configuracion.class)) }),
            @ApiResponse(responseCode = "400", description = "No se encontraron registros", content = @Content), 
            @ApiResponse(responseCode = "404", description = "Error retornado por el SP", content = @Content) }) 
	@PostMapping(value = "/consultarConfig", consumes = MediaType.APPLICATION_JSON_VALUE, produces =MediaType.APPLICATION_JSON_VALUE)
    public ConsultaConfigResponse consultarConfig(@RequestBody(description = "Request Consulta Configuracion", required = true,
            													content = @Content(
            													schema = @Schema(implementation = ConsultaConfigRequest.class)))
    											  @org.springframework.web.bind.annotation.RequestBody ConsultaConfigRequest request) throws JsonProcessingException {
		
		log.info("request" + request.toString());
        return configService.obtieneDatosConfigurativos(request.getTransaccion());
    }
}
