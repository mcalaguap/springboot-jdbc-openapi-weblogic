package pe.com.claro.jdbc.timeai.spconsultaconfiguracion.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Configuracion {

	private String confvTransaccion;
	private String confvParam;
	private String confvTipo;
	private String confvValueStr;
	private String confvDescripcion;
	private String confvValueNum;

}
