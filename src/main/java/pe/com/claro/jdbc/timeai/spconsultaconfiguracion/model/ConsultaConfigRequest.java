package pe.com.claro.jdbc.timeai.spconsultaconfiguracion.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ConsultaConfigRequest {

	private String transaccion;
}
