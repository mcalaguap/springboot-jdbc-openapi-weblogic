package pe.com.claro.jdbc.timeai.spconsultaconfiguracion.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.com.claro.jdbc.timeai.spconsultaconfiguracion.entity.Configuracion;

@Getter
@Setter
public class ConsultaConfigResponse {

	private List<Configuracion> datosConfiguracion;

}
