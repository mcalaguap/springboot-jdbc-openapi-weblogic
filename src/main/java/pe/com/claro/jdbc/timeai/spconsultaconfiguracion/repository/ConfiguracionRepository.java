package pe.com.claro.jdbc.timeai.spconsultaconfiguracion.repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.internal.OracleTypes;
import pe.com.claro.jdbc.timeai.spconsultaconfiguracion.common.PropiedadesExternas;
import pe.com.claro.jdbc.timeai.spconsultaconfiguracion.entity.Configuracion;

@Slf4j
@Repository
public class ConfiguracionRepository {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private PropiedadesExternas propiedadesExternas;

    private SimpleJdbcCall simpleJdbcCallRefCursor;
	
	@PostConstruct
    public void init() {
        jdbcTemplate.setResultsMapCaseInsensitive(true);

        simpleJdbcCallRefCursor = new SimpleJdbcCall(jdbcTemplate)
                .withoutProcedureColumnMetaDataAccess()
				.withSchemaName(propiedadesExternas.getSpOwner())
				.withProcedureName(propiedadesExternas.getSpName())
				.declareParameters(
						new SqlParameter("PIV_TRANS_NEG", OracleTypes.VARCHAR),
						new SqlOutParameter("K_DET_CURSOR", OracleTypes.CURSOR, BeanPropertyRowMapper.newInstance(Configuracion.class)),
					    new SqlOutParameter("PO_COD_RPTA", OracleTypes.NUMBER),
						new SqlOutParameter("PO_MSJ_RPTA", OracleTypes.VARCHAR));
       
        simpleJdbcCallRefCursor.getJdbcTemplate().setQueryTimeout(propiedadesExternas.getSpTimeout());
    }
	
	public List<Configuracion> obtieneDatosConfigurativos(String transaccion) {

        log.info("spowner: " +propiedadesExternas.getSpOwner());
        log.info("spname:" + propiedadesExternas.getSpName());
        log.info("sptimeout:" + propiedadesExternas.getSpTimeout());
        log.info("PIV_TRANS_NEG:" + transaccion);

        SqlParameterSource paramaters = new MapSqlParameterSource()
        							.addValue("PIV_TRANS_NEG", transaccion);

        Map<String, Object> resultMap = simpleJdbcCallRefCursor.execute(paramaters);
        
        log.info("PO_COD_RPTA:" + resultMap.get("PO_COD_RPTA"));
        log.info("PO_MSJ_RPTA:" + resultMap.get("PO_MSJ_RPTA"));

        if (resultMap == null) {
            return Collections.emptyList();
        } else {
            return (List) resultMap.get("K_DET_CURSOR");
        }


    }
}
