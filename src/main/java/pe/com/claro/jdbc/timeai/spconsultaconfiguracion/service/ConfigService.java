package pe.com.claro.jdbc.timeai.spconsultaconfiguracion.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import pe.com.claro.jdbc.timeai.spconsultaconfiguracion.entity.Configuracion;
import pe.com.claro.jdbc.timeai.spconsultaconfiguracion.model.ConsultaConfigResponse;
import pe.com.claro.jdbc.timeai.spconsultaconfiguracion.repository.ConfiguracionRepository;

@Slf4j
@Service
public class ConfigService {

	@Autowired
    private ConfiguracionRepository configuracionRepository;
	
	private ObjectMapper objectMapper;

	
	public ConsultaConfigResponse obtieneDatosConfigurativos(String transaccion) throws JsonProcessingException {
		
		log.info("Input:" + transaccion);
		ConsultaConfigResponse response = new ConsultaConfigResponse();
		List<Configuracion> result= configuracionRepository.obtieneDatosConfigurativos(transaccion);
		objectMapper = new ObjectMapper();

		log.info("output: " + objectMapper.writeValueAsString(response));

		response.setDatosConfiguracion(result);
		
		return response;
	}
}
